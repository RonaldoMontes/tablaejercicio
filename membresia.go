package main

import (
	"ejemplo/bd"

	"ejemplo/estructura"
	"encoding/json"
	"net/http"
)

func GetMembresiaEndPoint(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(bd.GetMembresia())
}

func PostMembresiaEndPoint(w http.ResponseWriter, r *http.Request) {
	var membresia estructura.Membresia
	json.NewDecoder(r.Body).Decode(&membresia)
	json.NewEncoder(w).Encode(bd.PostMembresia(membresia))

}
