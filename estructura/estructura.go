package estructura

type Membresia struct {
	IdMembresia   byte   `json:"idMembresia"`
	TipoMembresia string `json:"tipoMembresia"`
}
