package bd

import "Ejemplo/estructura"

func GetMembresia() (membresias []estructura.Membresia) {
	var (
		idMembresia   byte
		tipoMembresia string
	)

	membresia, _ := db.Query("SELECT IdMembresia, TipoMembresia FROM Membresia")
	revisarError(err)
	for membresia.Next() {

		err = membresia.Scan(
			&idMembresia,
			&tipoMembresia,
		)
		revisarError(err)

		membresias = append(membresias, estructura.Membresia{
			IdMembresia:   idMembresia,
			TipoMembresia: tipoMembresia,
		})
	}

	return
}

func PostMembresia(membresia estructura.Membresia) error {
	_, err := db.Exec("INSERT INTO Membresia(TipoMembresia) VALUES (?)", membresia.TipoMembresia)

	return err
}
