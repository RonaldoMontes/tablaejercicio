package main

import (
	"ejemplo/bd"

	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {

	api()
}

func api() {
	bd.NuevaConexionBD()
	defer bd.TerminarConcexionBD()

	gorillaRoute := mux.NewRouter().StrictSlash(true)

	gorillaRoute.HandleFunc("/api/get-membresia", bienhechores.GetMembresiaEndPoint).Methods("GET")
	gorillaRoute.HandleFunc("/api/post-membresia", bienhechores.PostMembresiaEndPoint).Methods("POST")

	gorillaRoute.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))

	http.Handle("/", gorillaRoute)

	log.Fatal(http.ListenAndServe(":9090", gorillaRoute))
}
